
/**
 * This class manages the container element for Deface posts. It parses
 * content from Facebook posts, and renders an iframe that contains the
 * necessary parameters for the background side to decrypt messages.
 *
 * Decryption-sandbox.html points to src/decryption-sandbox/index.html.
 */
export default class DecryptionFrame {
  /**
   * Create a DecryptionFrame.
   */
  constructor () {
    this.init = this.init.bind(this)
    this.renderFrame = this.renderFrame.bind(this)
  }

  init (id, element, content) {
    this.id = id
    this.element = element
    this.content = content

    // const regexp = /\"?([a-zA-Z0-9]{30})\"?<br>\s*\"?\s*([a-z0-9]+)\"?/
    const regexp = /([0-9a-fA-F]{40})\s+([0-9a-fA-F]+)\s+([0-9a-fA-F]{4})\s+([0-9]{1,3})/
    const match = this.content.match(regexp)
    this.keyHash = match[1]
    this.cipherText = match[2]
    this.iv = match[3]
    this.characterSetLength = match[4]

    window.chrome.runtime.sendMessage(
      {
        type: 'get-decryption-sandbox-size',
        id
      },
      (response) => {
        if (!response) {
          return console.error('empty response')
        }

        const {
          height,
          error
        } = response

        if (error || typeof height !== 'number') {
          return console.error(error || 'Invalid frame size')
        }

        this.frame.style.height = `${height}px`
        this.dialog.style.height = `${height}px`
      }
    )

    this.renderFrame(id)

    return this
  }

  /**
   * Render a container element sized as the initial cipherText post,
   * and insert an iframe that points to src/decryption-sandbox/index.html.
   */
  renderFrame (id) {
    const boundingBox = this.element.getBoundingClientRect()

    this.frame = document.createElement('div')
    this.frame.setAttribute('id', `frame-${this.keyHash}`)
    this.frame.className = 'decryption-frame'
    this.frame.style.width = `${boundingBox.width}px`
    this.frame.style.height = `${boundingBox.height}px`
    // this.frame.style.transition = 'height 0.15s'

    this.element.innerHTML = ''
    this.element.appendChild(this.frame)

    this.dialog = document.createElement('iframe')
    this.dialog.setAttribute('id', `dialog-${this.keyHash}`)
    this.dialog.className = 'decryption-frame-dialog'
    this.dialog.setAttribute('frameBorder', 0)
    this.dialog.setAttribute('scrolling', 'no')
    this.dialog.style.border = 'none'
    this.dialog.style.width = '100%'
    this.dialog.style.height = `${boundingBox.height}px`
    // this.dialog.style.transition = 'height 0.15s'

    const params = `id=${this.id}&keyHash=${this.keyHash}&iv=${this.iv}&charsetlength=${this.characterSetLength}&ciphertext=${this.cipherText}`
    const url = window.chrome.runtime.getURL(`decryption-sandbox.html?${params}`)
    this.dialog.setAttribute('src', url)
    this.frame.append(this.dialog)
  }
}
