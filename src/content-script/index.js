
'use strict'

import ContentScript from './ContentScript.js'

window.chrome.runtime.sendMessage({ type: 'isReady' }, (response) => {
  if (!response) {
    return console.error('empty response from Deface background')
  }

  if (response.ready) {
    new ContentScript().init()
  }
})
