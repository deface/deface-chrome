
import defaceIcon from '../_img/icon-38.png'

export default class ComposerButton {
  constructor (element) {
    this.element = null
    this.initialized = false

    this.init = this.init.bind(this)
    this.show = this.show.bind(this)
    this.hide = this.hide.bind(this)
    this.start = this.start.bind(this)
    this.close = this.close.bind(this)

    this.render = this.render.bind(this)
    this.onTooltipMouseOver = this.onTooltipMouseOver.bind(this)
    this.onTooltipMouseOut = this.onTooltipMouseOut.bind(this)
    this.onCloseButtonMouseOver = this.onCloseButtonMouseOver.bind(this)
    this.onCloseButtonMouseOut = this.onCloseButtonMouseOut.bind(this)
  }

  init (element) {
    this.element = element
    const isFocused = this.element === document.activeElement

    if (isFocused) {
      this.render()
    }

    this.element.addEventListener('focus', this.show)
    this.element.addEventListener('blur', this.hide)

    return this
  }

  render () {
    // TODO - prevent FB from applying changes to this container

    this.isCommentTextbox = false
    let e = this.element
    while (e !== document.body) {
      if (e.className.indexOf('_7cqq') > -1) {
        console.log(e, e.getAttribute('role'))
      }
      if (e.getAttribute('role') === 'article') {
        this.isCommentTextbox = true
        break
      }

      e = e.parentNode
    }

    this.tooltip = document.createElement('div')
    this.tooltip.className = 'deface-tooltip'
    this.tooltip.style.display = 'flex'
    this.tooltip.style.justifyContent = 'space-between'
    this.tooltip.style.cursor = 'auto'
    this.tooltip.style.backgroundColor = !this.isCommentTextbox ? 'rgb(245, 247, 250)' : 'white'
    this.tooltip.style.border = '1px solid rgb(193, 201, 230)'
    this.tooltip.style.borderRadius = '3px'
    this.tooltip.style.boxShadow = 'rgba(77, 68, 156, 0.15) 0px 2px 10px 0px'
    this.tooltip.style.position = 'absolute'
    this.tooltip.style.top = 0
    this.tooltip.style.right = 0
    this.tooltip.style.padding = '5px'
    this.tooltip.style.marginTop = !this.isCommentTextbox ? 0 : '-7px'
    this.tooltip.style.transition = 'opacity 0.1s'

    if (this.isCommentTextbox) {
      this.tooltip.style.opacity = 0.6
      this.tooltip.addEventListener('mouseover', this.onTooltipMouseOver)
      this.tooltip.addEventListener('mouseout', this.onTooltipMouseOut)
    }

    this.startButton = document.createElement('div')
    this.startButton.style.width = !this.isCommentTextbox ? '38px' : '19px'
    this.startButton.style.height = !this.isCommentTextbox ? '38px' : '19px'
    this.startButton.style.backgroundImage = `url(${window.chrome.extension.getURL(defaceIcon)})`
    this.startButton.style.backgroundSize = 'contain'
    this.startButton.style.marginRight = '5px'
    this.startButton.style.cursor = 'pointer'
    this.tooltip.appendChild(this.startButton)

    this.closeButton = document.createElement('div')
    this.closeButton.style.width = '9px'
    this.closeButton.style.height = '9px'
    this.closeButton.style.cursor = 'pointer'
    this.closeButton.style.opacity = 0.5
    this.closeButton.style.transition = 'opacity 0.1s'
    this.closeButton.innerHTML = `
      <svg viewPort="0 0 9 9" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: block;">
        <line
          x1="1"
          y1="8" 
          x2="8"
            y2="1"
          stroke="rgb(96, 100, 115)"
          stroke-width="2"
          style="pointer-events: none;"
        />
        <line x1="1"
          y1="1"
          x2="8"
          y2="8"
          stroke="rgb(96, 100, 115)"
          stroke-width="2"
          style="pointer-events: none;"
        />
      </svg>
    `
    this.closeButton.addEventListener('mouseover', this.onCloseButtonMouseOver)
    this.closeButton.addEventListener('mouseout', this.onCloseButtonMouseOut)
    this.tooltip.appendChild(this.closeButton)

    this.element.parentNode.appendChild(this.tooltip)

    this.startButton.addEventListener('mousedown', this.start)
    this.closeButton.addEventListener('mousedown', this.close)

    this.initialized = true
  }

  show (event) {
    if (!this.initialized) {
      this.render()
    }

    this.tooltip.style.display = 'flex'
  }

  hide (event) {
    this.tooltip.style.display = 'none'
  }

  start (event) {
    window.chrome.runtime.sendMessage(
      {
        type: 'composer'
      },
      (response) => {
        if (!response) {
          return console.error('Empty response after posting.')
        }

        const {
          error,
          keyHash,
          iv,
          characterSetLength,
          cipherText
        } = response

        if (error) {
          return console.error('Error while posting:', error)
        }

        if (!keyHash) {
          return null
        }

        const link = !this.isCommentTextbox ? `Deface: https://deface.app` : `Deface.`
        const postContent = `This message was encrypted with ${link} ${keyHash} ${cipherText} ${iv} ${characterSetLength}`
        const inputElement = this.element.querySelector('*[data-text]')
        inputElement.click()

        if (inputElement.tagName === 'BR') {
          const text = document.createTextNode(postContent)
          inputElement.parentNode.appendChild(text)
        } else {
          inputElement.innerText = postContent
        }

        const inputEvent = new window.CustomEvent('input', { bubbles: true })
        inputElement.dispatchEvent(inputEvent)

        if (inputElement.tagName === 'BR') {
          this.element.querySelector('*[data-text]').parentNode.removeChild(this.element.querySelector('*[data-text]').parentNode.childNodes[0])
        }
      }
    )
  }

  onTooltipMouseOver () {
    this.tooltip.style.opacity = 1
  }

  onTooltipMouseOut () {
    this.tooltip.style.opacity = 0.6
  }

  onCloseButtonMouseOver () {
    this.closeButton.style.opacity = 1
  }

  onCloseButtonMouseOut () {
    this.closeButton.style.opacity = 0.5
  }

  close () {
    if (this.isCommentTextbox) {
      this.tooltip.removeEventListener('mouseover', this.onTooltipMouseOver)
      this.tooltip.removeEventListener('mouseout', this.onTooltipMouseOut)
    }

    this.closeButton.removeEventListener('mouseover', this.onCloseButtonMouseOver)
    this.closeButton.removeEventListener('mouseout', this.onCloseButtonMouseOut)
    this.element.removeEventListener('focus', this.show)
    this.element.removeEventListener('blur', this.hide)
    this.startButton.removeEventListener('click', this.start)
    this.closeButton.removeEventListener('mousedown', this.close)

    this.tooltip.parentNode.removeChild(this.tooltip)
  }
}
