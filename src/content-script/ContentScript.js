

import ComposerButton from './ComposerButton'
import DecryptionFrame from './DecryptionFrame'

/**
 * This class manages interactions between Deface and the Facebook
 * page after it was injected with this script.
 *
 * It tracks changes to the Facebook feed DOM to detect Deface
 * posts and inject DecryptionFrame objects that display resulting plaintext.
 *
 * It also captures new messages submitted by the user to encrypt them
 * before sending them to Facebook.
 */
export default class ContentScript {
  /**
   * Create a ContentScript.
   */
  constructor () {
    /**
     * DOM element for the Facebook post composer.
     *
     * @type {Object}
     */
    this.composerElement = null

    /**
     * DOM element for the Facebook feed.
     *
     * @type {Object}
     */
    this.feedElement = null

    /**
     * DOM element for the submit button, part of the Facebook post composer.
     *
     * @type {Object}
     */
    this.submitElement = null

    /**
     * MutationObserver that keeps track of changes in the #content div of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.contentMutationObserver = null

    /**
     * MutationObserver that keeps track of changes in the composer element of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.composerMutationObserver = null

    /**
     * MutationObserver that keeps track of changes in the feed element of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.feedMutationObserver = null

    /**
     * Facebook post DOM elements captured in the feed.
     *
     * @type {Array.<Object>}
     */
    this.postElements = []
    this.textBoxElements = []

    /**
     * A list of all DecryptionFrame objects injected in the feed to replace encrypted posts.
     *
     * @type {Array.<DecryptionFrame>}
     */
    this.decryptionFrames = []
    this.composerButtons = []

    this.init = this.init.bind(this)
    this.onDOMMutated = this.onDOMMutated.bind(this)
    this.getNewPosts = this.getNewPosts.bind(this)
    this.getTextBoxes = this.getTextBoxes.bind(this)
  }

  init () {
    const textBoxElements = Array.prototype.slice.call(document.querySelectorAll('*[role="textbox"]'))
    if (textBoxElements.length > 0) {
      this.getTextBoxes([textBoxElements], [])
    }

    const postElements = Array.prototype.slice.call(document.querySelectorAll('*[role="article"]'))
    if (postElements.length > 0) {
      this.getNewPosts([postElements], [])
    }

    const mutationObserver = new window.MutationObserver(this.onDOMMutated)
    mutationObserver.observe(document.body, { childList: true, attributes: true, subtree: true })

    return this
  }

  onDOMMutated (mutationsList) {
    // TODO:
    // attributes could be added when added as well
    // style can change as well

    const childMutations = mutationsList.filter(mutation => mutation.type === 'childList')
      .map(mutation => Array.prototype.slice.call(mutation.addedNodes).filter(node => node.nodeType === 1))
      .filter(nodes => nodes.length > 0)

    const roleMutations = mutationsList.filter(mutation => mutation.attributeName === 'role')

    this.getTextBoxes(childMutations, roleMutations)
    this.getNewPosts(childMutations, roleMutations)
  }

  getTextBoxes (childMutations, roleMutations) {
    const textBoxChildMutations = childMutations.reduce((list, nodes) => {
      nodes
        .map(element => {
          return element.getAttribute('role') === 'textbox'
            ? [element]
            : Array.prototype.slice.call(element.querySelectorAll('*[role="textbox"]'))
        })
        .reduce((list, elements) => {
          elements.forEach(element => {
            list.push(element)
          })
          return list
        })
        .filter(element => element)
        .forEach(element => {
          list.push(element)
        })
      return list
    }, [])

    const textBoxRoleMutations = roleMutations
      .filter(mutation => mutation.target.getAttribute('role') === 'textbox')
      .map(mutation => mutation.target)

    const newTextBoxes = textBoxChildMutations
      .concat(textBoxRoleMutations)
      .filter(element => this.textBoxElements.indexOf(element) === -1)
      .reduce((list, element) => {
        if (list.indexOf(element) === -1) {
          list.push(element)
        }
        return list
      }, [])

    newTextBoxes.forEach(element => {
      this.textBoxElements.push(element)
      const composerButton = new ComposerButton().init(element)
      this.composerButtons.push(composerButton)
    })
  }

  getNewPosts (childMutations, roleMutations) {
    const postChildMutations = childMutations.reduce((list, nodes) => {
      nodes
        .map(element => {
          return element.getAttribute('role') === 'article'
            ? [element]
            : Array.prototype.slice.call(element.querySelectorAll('*[role="article"]'))
        })
        .reduce((list, elements) => {
          elements.forEach(element => {
            list.push(element)
          })
          return list
        })
        .filter(element => element)
        .forEach(element => {
          list.push(element)
        })
      return list
    }, [])

    const postRoleMutations = roleMutations
      .filter(mutation => mutation.target.getAttribute('role') === 'article')
      .map(mutation => mutation.target)

    const newPosts = postChildMutations
      .concat(postRoleMutations)
      .filter(element => this.postElements.indexOf(element) === -1)
      .reduce((list, element) => {
        if (list.indexOf(element) === -1) {
          list.push(element)
        }
        return list
      }, [])

    // deface keyHash encryptedText iv charset 
    const regexp = /Deface.*?[0-9a-fA-F]{40}\s+[0-9a-fA-F]+\s+[0-9a-fA-F]{4}\s+[0-9]{1,3}/

    newPosts.forEach(element => {
      const children = Array.prototype.slice.call(element.querySelectorAll('*'))

      const nestedArticles = Array.prototype.slice.call(element.querySelectorAll('*[role="article"]'))

      // TODO
      const visibleElements = children
        .filter(element => {
          const bbox = element.getBoundingClientRect()
          // const isvis = isVisible(element)
          // return isvis
          // return isVisible(element) && bbox.width >= 2 && bbox.height >= 2
          // return true
          return bbox.width >= 2 && bbox.height >= 2
        })

      const visibleNodes = visibleElements.reduce((list, element) => {
        Array.prototype.slice.call(element.childNodes)
          .filter(node => node.nodeType === 3)
          .forEach(node => {
            list.push(node)
          })
        return list
      }, [])

      const rootNodes = nestedArticles.length === 0 ? visibleNodes : visibleNodes
        .filter(node => {
          let n = node
          while (n && n !== element) {
            if (nestedArticles.indexOf(n) > -1) {
              return false
            }
            n = n.parentNode
          }
          return true
        })

      // TODO: facebook could technically split up nodes that appear as unreadable to humans?
      const contentNodes = rootNodes
        .filter(node => node.data.length > 0)

      const nodeCharacterIndices = []

      let i = 0
      contentNodes.forEach(node => {
        nodeCharacterIndices.push(i)
        i += node.data.length + 1
      })

      const content = contentNodes.map(node => node.data).join(' ')
      const match = content.match(regexp)

      if (match && match.length === 1) {
        const contentStartIndex = content.indexOf(match[0])
        const contentEndIndex = contentStartIndex + match[0].length

        let contentStartNode = null
        let contentEndNode = null

        for (let i = 0; i < contentNodes.length; i++) {
          if (nodeCharacterIndices[i] > contentStartIndex) {
            contentStartNode = contentNodes[i - 1]
            break
          }
        }

        for (let i = contentNodes.length - 1; i >= 0; i--) {
          if (nodeCharacterIndices[i] <= contentEndIndex) {
            contentEndNode = contentNodes[i]
            break
          }
        }

        let defaceElement = null
        let n1 = contentStartNode
        let n2 = contentEndNode

        const contentNodeTree = {}

        while (n1) {
          if (n1.nodeType === 1) {
            contentNodeTree[n1] = true
          }

          n1 = n1.parentNode
        }

        while (n2) {
          if (contentNodeTree[n2]) {
            defaceElement = n2
            break
          }

          n2 = n2.parentNode
        }

        let decryptionFrame = this.decryptionFrames.find(p => p.element === defaceElement)

        if (!decryptionFrame) {
          const content = match[0]
          const id = this.decryptionFrames.length
          decryptionFrame = new DecryptionFrame().init(id, defaceElement, content)
          this.decryptionFrames.push(decryptionFrame)
        }
      }

      this.postElements.push(element)
    })
  }
}
