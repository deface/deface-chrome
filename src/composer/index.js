
import Composer from './Composer'

const query = new URLSearchParams(document.location.search)
const requestId = query.get('requestId')

const composerElement = document.getElementById('composer')
const submitElement = document.getElementById('submit')
const exitElement = document.getElementById('exit-button')

new Composer(requestId, composerElement, submitElement, exitElement).init()
