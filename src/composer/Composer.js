
export default class Composer {
  constructor (requestId, composerElement, submitElement, exitElement) {
    this.defaultPostContent = `What's on your mind?`
    this.postContent = this.defaultPostContent
    this.requestId = requestId
    this.composerElement = composerElement
    this.submitElement = submitElement
    this.exitElement = exitElement
    this.complexity = 1
    this.alpha = 0
    this.speed = 0.15

    this.init = this.init.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.setComplexity = this.setComplexity.bind(this)
    this.onComposerFocus = this.onComposerFocus.bind(this)
    this.onComposerBlur = this.onComposerBlur.bind(this)
    this.onComposerInput = this.onComposerInput.bind(this)
    this.animate = this.animate.bind(this)
    this.exit = this.exit.bind(this)
    this.setAverageTime = this.setAverageTime.bind(this)
  }

  init () {
    this.composerElement.innerText = this.postContent
    this.composerElement.addEventListener('focus', this.onComposerFocus)
    this.composerElement.addEventListener('blur', this.onComposerBlur)
    this.composerElement.addEventListener('input', this.onComposerInput)

    this.exitElement.addEventListener('click', this.exit)

    this.submitElement.addEventListener('click', this.onSubmit)

    this.complexityOptions = Array.prototype.slice.call(document.querySelectorAll('#complexity #selector .radio'))
    this.complexityOptions.forEach((radio, i) => {
      radio.addEventListener('click', () => {
        this.setComplexity(i + 1)
      })
    })

    const documentSize = document.body.getBoundingClientRect()
    window.resizeTo(documentSize.width, documentSize.height + 20)

    this.averageTimeCounter = document.querySelector('#complexity #help span')

    window.chrome.runtime.sendMessage(
      { type: 'measure-performance' },
      (response) => {
        console.log('RESPONSE', response)
        if (!response) {
          return this.setAverageTime()
          // return averageTimeCounter.innerText = '- seconds'
        }

        const {
          error,
          operationCount
        } = response

        if (error || !operationCount) {
          return this.setAverageTime()
          // return averageTimeCounter.innerText = '- seconds'
        }

        this.operationCountPerSecond = operationCount
        this.setAverageTime()
      }
    )

    return this
  }

  setAverageTime () {
    if (!this.operationCountPerSecond) {
      this.averageTimeCounter.innerText = '- seconds'
      return null
    }

    const keyLength = 3
    const characterSetLength = Math.pow(2, 4 + this.complexity)
    const averageOperationCount = Math.pow(characterSetLength, keyLength) / 2

    let unit = 'seconds'
    let time = averageOperationCount / this.operationCountPerSecond
    if (time >= 60) {
      unit = 'minutes'
      time /= 60
    }

    if (time < 10) {
      time = Math.floor(time * 10) / 10
    } else {
      time = Math.floor(time)
    }

    if (time < 2) {
      unit = unit.slice(0, -1)
    }

    this.averageTimeCounter.innerText = `${time} ${unit}`
  }

  setComplexity (complexity) {
    this.complexity = complexity
    this.setAverageTime()
  }

  onComposerFocus (event) {
    if (this.composerElement.innerText === this.defaultPostContent) {
      this.composerElement.innerText = ''
      this.postContent = ''
    }

    this.composerElement.style.color = 'black'
  }

  onComposerBlur (event) {
    if (this.composerElement.innerText === '') {
      this.composerElement.innerText = this.defaultPostContent
      this.postContent = this.defaultPostContent
      this.composerElement.style.color = '#90949c'
    }
  }

  onComposerInput (event) {
    this.postContent = this.composerElement.innerText
    if (!this.postContent || this.postContent === this.defaultPostContent) {
      this.submitElement.style.opacity = 0.4
      this.submitElement.style.pointerEvents = 'none'
      this.submitElement.style.cursor = 'auto'
    } else {
      this.submitElement.style.opacity = 1
      this.submitElement.style.pointerEvents = 'auto'
      this.submitElement.style.cuursor = 'pointer'
    }

    if (this.postContent.length < 85) {
      this.composerElement.style.fontSize = '24px'
      this.composerElement.style.lineHeight = '28px'
    } else {
      this.composerElement.style.fontSize = '14px'
      this.composerElement.style.lineHeight = '18px'
    }
  }

  onSubmit () {
    if (!this.postContent || this.postContent === this.defaultPostContent) {
      return null
    }

    const characterSetLength = Math.pow(2, 4 + this.complexity)

    const params = {
      type: 'submit',
      requestId: this.requestId,
      plainText: this.postContent,
      characterSetLength
    }

    window.chrome.runtime.sendMessage(
      params,
      (response) => {
        if (!response) {
          // TODO error message
          return window.close()
        }

        const {
          error,
          keyHash,
          iv,
          characterSetLength,
          cipherText
        } = response

        if (error || !keyHash) {
          // TODO error message
          return window.close()
        }

        this.encryptedContent = `This message was encrypted with Deface. ${keyHash} ${cipherText} ${iv} ${characterSetLength}`
        document.body.style.pointerEvents = 'none'
        this.animate()
      }
    )
  }

  animate () {
    const missingCharacterCount = this.encryptedContent.length - this.postContent.length

    if (missingCharacterCount > 0) {
      for (let i = 0; i < missingCharacterCount; i++) {
        if (Math.random() < this.alpha / 2) {
          this.postContent += Math.random().toString(36).slice(2)[0]
        }
      }
    }

    const text = Array.prototype.slice.call(this.postContent)
      .map((c, i) => {
        if (this.encryptedContent.charAt(i) === c || Math.random() < this.alpha) {
          return this.encryptedContent.charAt(i)
        }
        return Math.random().toString(36).slice(2)[0]
      })
      .join('')

    this.alpha = (1 - this.alpha) * this.speed

    this.postContent = text
    this.composerElement.innerText = text

    if (this.postContent.length < 85) {
      this.composerElement.style.fontSize = '24px'
      this.composerElement.style.lineHeight = '28px'
    } else {
      this.composerElement.style.fontSize = '14px'
      this.composerElement.style.lineHeight = '18px'
    }

    if (text === this.encryptedContent) {
      window.setTimeout(() => {
        document.body.style.opacity = 0
        window.setTimeout(() => {
          window.close()
        }, 750)
      }, 750)
    } else {
      window.requestAnimationFrame(this.animate)
    }
  }

  exit () {
    console.log('exit')

    const params = {
      type: 'exit-composer',
      requestId: this.requestId
    }

    window.chrome.runtime.sendMessage(
      params,
      () => {
        window.close()
      }
    )
  }
}
