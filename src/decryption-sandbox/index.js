
import DecryptionSandbox from './DecryptionSandbox'

const element = document.createElement('div')
document.body.appendChild(element)

const query = new URLSearchParams(document.location.search)
const id = query.get('id')
const keyHash = query.get('keyHash')
const iv = query.get('iv')
const characterSetLength = parseInt(query.get('charsetlength'))
const cipherText = query.get('ciphertext')

new DecryptionSandbox(element, id, keyHash, iv, characterSetLength, cipherText).init()
