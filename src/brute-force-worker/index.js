
import forge from 'node-forge'

const asciiCharacters = ['']
  .concat(
    new Array(256)
      .fill(0)
      .map((v, i) => String.fromCharCode(i))
  )

asciiCharacters.unshift('')

function intToString (value, toBase) {
  var newValue = ''
  while (value > 0) {
    newValue = asciiCharacters[value % toBase] + newValue
    value = (value - (value % toBase)) / toBase
  }
  return newValue
}

function bruteForceMessage (data) {
  const {
    keyHash,
    characterSetLength,
    timeout
  } = data

  const keyLength = 3

  const startDate = Date.now()
  let i = Math.pow(characterSetLength, keyLength)

  while (i-- > 0) {
    const key = intToString(i, characterSetLength)

    const candidateDigest = forge.md.sha1.create()
    candidateDigest.update(key)
    const candidateHash = candidateDigest.digest().toHex()

    if (keyHash === candidateHash) {
      return postMessage({
        key
      })
    }
    // else if (Math.random() < 0.0001 && Date.now() - startDate > timeout) {
    //   break
    // }
  }

  return postMessage({
    error: new Error('Could not brute force key.')
  })
}

function measurePerformance (data) {
  const {
    timeout
  } = data

  const keyLength = 3
  const characterSetLength = 64

  const startDate = Date.now()
  const length = Math.pow(characterSetLength, keyLength)
  let operationCount = 1

  const impossibleKey = new Array(3).fill(asciiCharacters[characterSetLength]).join('')
  const impossibleKeyDigest = forge.md.sha1.create()
  impossibleKeyDigest.update(impossibleKey)
  const impossibleHash = impossibleKeyDigest.digest().toHex()

  while (operationCount++) {
    const key = intToString(operationCount % length, characterSetLength)

    const candidateDigest = forge.md.sha1.create()
    candidateDigest.update(key)
    const candidateHash = candidateDigest.digest().toHex()

    if (impossibleHash === candidateHash) {
      //
    } else if (Math.random() < 0.001 && Date.now() - startDate > timeout) {
      break
    }
  }

  postMessage({
    operationCount
  })
}

onmessage = function (event) {
  switch (event.data.type) {
    case 'message' :
      bruteForceMessage(event.data)
      break
    case 'performance' :
      measurePerformance(event.data)
      break
  }
}
