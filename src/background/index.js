'use strict'

import KeychainManager from './KeychainManager.js'

import '../_img/icon-16.png'
import '../_img/icon-19.png'
import '../_img/icon-38.png'
import '../_img/icon-48.png'
import '../_img/icon-128.png'

class Deface {
  constructor () {
    this.composerRequestIndex = 0
    this.composerRequests = {}
    this.sizeRequests = {}

    this.keychainManager = new KeychainManager()
    this.onMessage = this.onMessage.bind(this)
    this.openComposer = this.openComposer.bind(this)
    this.submit = this.submit.bind(this)
    this.addDecryptionSandboxSizeRequest = this.addDecryptionSandboxSizeRequest.bind(this)
    this.resizeDecryptionFrame = this.resizeDecryptionFrame.bind(this)
    this.exitComposer = this.exitComposer.bind(this)

    window.chrome.runtime.onMessage.addListener(this.onMessage)
  }

  onMessage (request, sender, callback) {
    // TODO check for sender.tab.url.indexOf('facebook.com') > -1
    if (sender.tab) {
      switch (request.type) {
        case 'isReady' :
          const res = { ready: true }
          callback(res)
          break
        case 'decrypt' :
          this.keychainManager.addRequestToQueue(request.keyHash, request.iv, request.characterSetLength, request.cipherText, sender.tab.id, callback)
          break
        case 'composer' :
          this.openComposer(sender.tab.id, callback)
          break
        case 'submit' :
          this.submit(request.plainText, request.characterSetLength, request.requestId, callback)
          break
        case 'exit-composer' :
          this.exitComposer(request.requestId, callback)
          break
        case 'get-decryption-sandbox-size' :
          this.addDecryptionSandboxSizeRequest(sender.tab.id, request.id, callback)
          break
        case 'resize-decryption-frame' :
          this.resizeDecryptionFrame(sender.tab.id, request.id, request.height)
          break
        case 'measure-performance' :
          this.keychainManager.measurePerformance(callback)
          break
        default :
          const emptyResponse = {}
          callback(emptyResponse)
      }
    }
    return true
  }

  addDecryptionSandboxSizeRequest (tabId, frameId, callback) {
    const id = `${tabId}-${frameId}`
    this.sizeRequests[id] = callback
  }

  resizeDecryptionFrame (tabId, frameId, height) {
    const id = `${tabId}-${frameId}`
    const callback = this.sizeRequests[id]

    if (callback) {
      try {
        window.chrome.tabs.get(tabId, tab => {
          if (tab) {
            const response = { height }
            callback(response)
          }
        })
      } catch (error) {}
    }
  }

  openComposer (tabId, callback) {
    const width = 640
    const height = 480
    const top = 50
    const left = 50
    const type = 'popup'

    this.composerRequests[this.composerRequestIndex] = callback

    const params = `requestId=${this.composerRequestIndex}`
    const url = window.chrome.runtime.getURL(`composer.html?${params}`)

    window.chrome.windows.create({ url, width, height, top, left, type, tabId })

    this.composerRequestIndex++
  }

  submit (plainText, characterSetLength, requestId, callback) {
    this.keychainManager.encryptAndStore(plainText, characterSetLength, (error, data) => {
      const {
        keyHash,
        iv,
        cipherText
      } = data

      const response = {
        error,
        keyHash,
        iv,
        characterSetLength,
        cipherText
      }

      if (this.composerRequests[requestId]) {
        this.composerRequests[requestId](response)
        delete this.composerRequests[requestId]
        callback(response)
      } else {
        callback(response)
      }
    })
  }

  exitComposer (requestId, callback) {
    if (this.composerRequests[requestId]) {
      const response = {}
      this.composerRequests[requestId](response)
      delete this.composerRequests[requestId]
    }

    callback()
  }
}

const deface = new Deface()
