
import { waterfall, filter } from 'async'
import { encrypt, decrypt } from './crypto.js'

export default class KeychainManager {
  constructor () {
    this.storageRequestQueue = []
    this.bruteForceRequestQueue = []

    this.isAStorageRequestProcessing = false
    this.isABruteForceRequestProcessing = false

    this.encryptAndStore = this.encryptAndStore.bind(this)
    this.addRequestToQueue = this.addRequestToQueue.bind(this)
    this.processNextStorageRequest = this.processNextStorageRequest.bind(this)
    this.processNextBruteForceRequest = this.processNextBruteForceRequest.bind(this)
    this.queryStorage = this.queryStorage.bind(this)
    this.bruteForce = this.bruteForce.bind(this)
    this.addKeyToStorage = this.addKeyToStorage.bind(this)
    this.getActiveContentScripts = this.getActiveContentScripts.bind(this)
    this.measurePerformance = this.measurePerformance.bind(this)
  }

  encryptAndStore (plainText, characterSetLength, callback) {
    waterfall(
      [
        (callback) => encrypt(plainText, characterSetLength, callback),
        this.addKeyToStorage
      ],
      callback
    )
  }

  addRequestToQueue (keyHash, iv, characterSetLength, cipherText, tabId, callback) {
    const allRequests = this.storageRequestQueue.concat(this.bruteForceRequestQueue)
    const request = allRequests.find(request => request.keyHash === keyHash)

    if (!request) {
      const script = {
        tabId,
        callback
      }

      const request = {
        keyHash,
        iv,
        characterSetLength,
        cipherText,
        completed: false,
        scripts: [script]
      }

      this.storageRequestQueue.push(request)
    } else {
      const script = {
        tabId,
        callback
      }

      request.scripts.push(script)
    }

    this.processNextStorageRequest()
  }

  processNextStorageRequest () {
    if (this.isAStorageRequestProcessing) {
      return null
    }

    const request = this.storageRequestQueue.pop()

    if (!request) {
      return null
    }

    this.isAStorageRequestProcessing = true
    // console.log('processing storage request', request.keyHash)

    waterfall(
      [
        (callback) => this.queryStorage(request, callback),
        (key, callback) => decrypt(request, key, callback),
        (data, callback) => {
          this.getActiveContentScripts(request, (error, activeScripts) => {
            if (error) {
              console.error(error)
            }

            const {
              plainText
            } = data

            activeScripts.forEach((script, i) => {
              script.callback({ plainText })
            })

            callback()
          })
        }
      ],
      (error) => {
        if (error) {
          this.bruteForceRequestQueue.push(request)
          this.processNextBruteForceRequest()
        }

        this.isAStorageRequestProcessing = false
        this.processNextStorageRequest()
      }
    )
  }

  processNextBruteForceRequest () {
    if (this.isABruteForceRequestProcessing) {
      return null
    }

    const request = this.bruteForceRequestQueue.pop()

    if (!request) {
      return null
    }

    this.isABruteForceRequestProcessing = true
    // console.log('processing brute force request', request.keyHash)

    waterfall(
      [
        (callback) => this.getActiveContentScripts(request, callback),
        (activeScripts, callback) => {
          if (activeScripts.length === 0) {
            this.isABruteForceRequestProcessing = false
            return this.processNextBruteForceRequest()
          }

          this.bruteForce(request, callback)
        },
        (key, callback) => decrypt(request, key, callback),
        this.addKeyToStorage
      ],
      (error, data) => {
        const {
          plainText
        } = data

        this.getActiveContentScripts(request, (err, activeScripts) => {
          if (err) {
            console.error(err)
          }

          activeScripts.forEach(script => {
            script.callback({
              error,
              plainText
            })

            this.isABruteForceRequestProcessing = false
            this.processNextBruteForceRequest()
          })
        })
      }
    )
  }

  bruteForce (request, callback) {
    if (!window.Worker) {
      return callback(new Error('Server workers are not available.'))
    }

    const {
      keyHash,
      characterSetLength
    } = request

    const worker = new window.Worker('bruteforceworker.bundle.js')

    worker.onmessage = (event) => {
      const {
        error,
        key
      } = event.data

      worker.terminate()

      callback(error, key)
    }

    console.log('start bruteforce', keyHash, characterSetLength)

    worker.postMessage({
      keyHash,
      characterSetLength,
      timeout: 20000,
      type: 'message'
    })
  }

  measurePerformance (callback) {
    if (!window.Worker) {
      const response = {
        error: new Error('Server workers are not available.')
      }
      return callback(response)
    }

    const worker = new window.Worker('bruteforceworker.bundle.js')

    worker.onmessage = (event) => {
      const {
        error,
        operationCount
      } = event.data

      worker.terminate()

      const response = {
        error,
        operationCount
      }

      callback(response)
    }

    worker.postMessage({
      timeout: 1000,
      type: 'performance'
    })
  }

  queryStorage (request, callback) {
    const {
      keyHash
    } = request

    window.chrome.storage.sync.get(keyHash, storeValue => {
      const key = storeValue[keyHash]

      if (!key) {
        return callback(new Error('No key in storage.'))
      }

      callback(null, key)
    })
  }

  addKeyToStorage (data, callback) {
    const {
      keyHash,
      key
    } = data

    const storeValue = {
      [keyHash]: key
    }

    window.chrome.storage.sync.set(storeValue, (error) => {
      if (error) {
        console.error(error)
      }

      callback(null, data)
    })
  }

  getActiveContentScripts (request, callback) {
    filter(
      request.scripts,
      (script, callback) => {
        try {
          window.chrome.tabs.get(script.tabId, tab => {
            if (!tab) {
              return callback(null, false)
            }
            return callback(null, true)
          })
        } catch (error) {
          return callback(null, false)
        }
      },
      callback
    )
  }
}
