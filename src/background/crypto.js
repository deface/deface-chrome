
import forge from 'node-forge'
import randomHex from 'crypto-random-hex'

const asciiCharacters = new Array(256)
  .fill(0)
  .map((v, i) => String.fromCharCode(i))

function randomString (characterSetLength, stringLength) {
  const view = window.crypto.getRandomValues(new Uint8Array(stringLength))
  const characterSetScaleFactor = characterSetLength / 256
  let result = ''

  for (var i = 0; i < view.length; i++) {
    result += asciiCharacters[Math.floor(view[i] * characterSetScaleFactor)]
  }

  return result
}

export function encrypt (plainText, characterSetLength, callback) {
  // const bytes = Math.ceil(n / 2)
  // const key = randomHex(bytes).slice(0, n)
  // const iv = randomString(characterSetLength, keyLength)

  const keyLength = 3
  const key = randomString(characterSetLength, keyLength)
  const iv = randomHex(2)

  const cipher = forge.cipher.createCipher('DES-CBC', key)
  cipher.start({ iv })
  cipher.update(forge.util.createBuffer(plainText))
  cipher.finish()
  const cipherText = cipher.output.toHex()

  const keyDigest = forge.md.sha1.create()
  keyDigest.update(key)
  const keyHash = keyDigest.digest().toHex()

  const data = {
    keyHash,
    iv,
    key,
    cipherText,
    plainText
  }

  callback(null, data)
}

export function decrypt (request, key, callback) {
  const {
    cipherText,
    keyHash,
    iv
  } = request

  const decipher = forge.cipher.createDecipher('DES-CBC', key)
  decipher.start({ iv })
  const cipherBytes = forge.util.hexToBytes(cipherText)
  const cipherBuffer = forge.util.createBuffer(cipherBytes)
  decipher.update(cipherBuffer)
  const result = decipher.finish()

  if (!result) {
    return callback(new Error(`No result from decryption: ${key} ${cipherText}`))
  }

  let plainText = ''

  try {
    plainText = decipher.output.toString()
  } catch (error) {
    return callback(new Error(`Error while reading output: ${error}`))
  }

  const data = {
    keyHash,
    iv,
    key,
    cipherText,
    plainText
  }

  callback(null, data)
}
