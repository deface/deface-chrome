![Logo Deface](https://deface.app/images/deface-banner.jpg "Logo Deface")

# deface-chrome

Deface is an app that piggybacks on Facebook and obfuscates timeline content: it makes user messages costly for computers to read, while preserving people's user experience. To achieve this, it relies on a novel approach based on the concept of [proof of work](https://deface.app/docs#privacy).

[https://deface.app](https://deface.app)

## Install & Use

```
npm install
npm run build
```

You can then go to chrome://extensions, enable the developer mode, and load the unpacked build by pointing to the `build` folder freshly generated. This will load the extension in Chrome, which you can refresh after each new build.

## 🚨🚨🚨 Disclaimer 🚨🚨🚨

Deface currently exists in its alpha 4 version. It is not suitable for use by anyone. Please be patient and courteous: don't use Deface in the wild before it reaches a stable state.

Additionally, Deface was recently refactored. The code is currently messy and undocumented. Beware of the dragons 🐉🐉🐉

## Contribute

We need help, and welcome all contributions! If you're not sure where to start, reading through [the project's homepage](https://deface.app) and [https://gitlab.com/deface/deface](home repository) will be helpful. If you want to contribute with code, please have a look at our [issue board](https://gitlab.com/groups/deface/-/boards). Feel free to submit issues and (even better) merge requests. 

This project defaults to [standard](https://standardjs.com/) code style. It is a clean codestyle, and its adoption is increasing significantly, making the code that we write familiar to the majority of the developers.

## Code of Conduct

This project follows a [Code of Conduct](https://gitlab.com/deface/deface/blob/master/CODE-OF-CONDUCT.md) based on [NPM's](https://www.npmjs.com/policies/conduct).

## License

MIT